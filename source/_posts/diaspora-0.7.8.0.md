
---
layout: "post"
title: "diaspora* 0.7.8.0"
date: 2018-11-26
tags:
    - diaspora
preview: "New minor diaspora* release features full-resolution images, memory usage improvement, docker image and admin panel enhancement"
url: "https://pod.diaspora.software/posts/23e13bb0d3290136e42f5254001bd39e"
lang: en
---

New minor diaspora* release is out. This version lets you see full-resolution images by clicking on photos inside a post. The release also improves diaspora* memory usage, provides a docker-based setup, and adds the ability to assign administration and moderation roles from within admin panel.


[Official announcement](https://pod.diaspora.software/posts/23e13bb0d3290136e42f5254001bd39e) | [changelog](https://github.com/diaspora/diaspora/releases/tag/v0.7.8.0)

